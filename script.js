let boutonThem=document.querySelector("#mode");

let spanText=document.querySelector("span");

if(localStorage.getItem('theme')){
    if(localStorage.getItem("theme") == 'sombre' ){
        modeSombre();
    }
}

boutonThem.addEventListener('click',()=>{
    if(document.body.classList.contains('dark')){
        document.body.className="";
        spanText.textContent = "Theme sombre";
        localStorage.setItem('theme','clair');
    }
    else{
        modeSombre();
    }
})

function modeSombre(){
    document.body.className = 'dark';
    spanText.textContent = "Theme clair";
    localStorage.setItem('theme','sombre');
}

//console.log(spanText);